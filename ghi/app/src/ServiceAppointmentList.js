import React from 'react'

class ServiceAppointmentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           appointments: []
        };
    };

    async componentDidMount() {
        const url = 'http://localhost:8080/api/service/appointments/';
    
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          this.setState({ appointments: data.appointments });
        }
      };

    render() {
        return (
            <div>
                <h1>Service Appointments</h1>
               
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">VIN</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Technician</th>
                            <th scope="col">Reason</th>
                            {/* <th scope="col">Status</th> */}
                            <th scope="col">VIP</th>
                        </tr>
                    </thead>
                        <tbody>
                            {this.state.appointments.map(appointment => {
                                return (
                                    <tr>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.customer_name}</td>
                                        <td>{new Date(appointment.date_created).toLocaleDateString()}</td>
                                        <td>{new Date(appointment.date_created).toLocaleTimeString()}</td>
                                        <td>{appointment.technician.name}</td>
                                        <td>{appointment.appointment_reason}</td>
                                        {appointment.vip ? <td>VIP</td> : <td> </td>}
                                        {/* <td>{appointment.vip}</td> */}
                                    </tr>
                                )
                            })}
                        </tbody>
                </table>
            
            </div>
        );
    }
}
export default ServiceAppointmentList;