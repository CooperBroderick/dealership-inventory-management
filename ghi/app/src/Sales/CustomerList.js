import React from "react";

class CustomerList extends React.Component {
    constructor() {
        super();
        this.state = {
            recordlists: [],
        };
    }

    async componentDidMount() {
        const recordUrl = "http://localhost:8090/api/sales/";
        const response = await fetch(recordUrl);
        if (response.ok) {
            const data = await response.json();
            this.setState({ recordlists: data.recordlist });
        }
    }

    render() {
        return (
            <React.Fragment>
                <h1>Customer List</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Customer</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">Sales Person</th>
                            <th scope="col">Street Address</th>
                            <th scope="col">City</th>
                            <th scope="col">State</th>
                            <th scope="col">Zip Code</th>
                            <th scope="col">Country</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.recordlists.map((recordlist, index) => {
                            return (
                                <tr key={index}>
                                    <td>{recordlist.customer.name}</td>
                                    <td>{recordlist.customer.phonenumber}</td>
                                    <td>{recordlist.sales_person}</td>
                                    <td>{recordlist.customer.customer_address.address}</td>
                                    <td>{recordlist.customer.customer_address.city}</td>
                                    <td>{recordlist.customer.customer_address.state}</td>
                                    <td>{recordlist.customer.customer_address.zipcode}</td>
                                    <td>{recordlist.customer.customer_address.country}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </React.Fragment>
        );
    }
}

export default CustomerList