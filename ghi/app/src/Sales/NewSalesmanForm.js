import React from 'react';

class NewSalesPersonForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          employeeid: '',
        };
      }
  handleChange = event => {
    const {name, value} = event.target;
    this.setState({
        [name]: value
    });
  }
  handleSubmit = async event => {
    event.preventDefault();
    const data = {...this.state};
    const newSalesmanUrl = `http://localhost:8090/api/sales/employees/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(newSalesmanUrl, fetchConfig);
    if (response.ok) {
      const cleared = {
        name: '',
        employeeid: '',
      };
      this.setState(cleared);
    }
  }

    render(){
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Welcome to the team!</h1>
                <form onSubmit={this.handleSubmit} id="create-salesman-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.employeeid} placeholder="Employee ID" required type="text" name="employeeid" id="employeeid" className="form-control" />
                    <label htmlFor="employeeid">Employee ID</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )

    }
}

export default NewSalesPersonForm
