import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
      <NavLink className="navbar-brand" to="/">Car Car</NavLink>
        <NavLink className="navbar-brand" to="/service/">Service</NavLink>
        <NavLink className="navbar-brand" to="/service/new">Service Appointment</NavLink>
        <NavLink className="navbar-brand" to="/service/history">Service History</NavLink>
        <NavLink className="navbar-brand" to="/techician/new">Technician</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className='p-2 dropdown'>
          <NavLink
              className='btn btn-outline-success dropdown-toggle'
            to='#'
            role='button'
            id='dropdownMenu'
            data-bs-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            Inventory
          </NavLink>
          <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
            <NavLink className='dropdown-item' to='/listvehicle'>
              Vehicle Inventory
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/listmodels'>
              Model Inventory
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/listmanufacturers'>
              Manufacturer Inventory
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/addvehicle'>
              Log new vehicle
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/addmodel'>
              Log new model
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/addmanufacturer'>
              Log new manufacturer
            </NavLink>
          </div>
          </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <div className='p-2 dropdown'>
          <NavLink
              className='btn btn-outline-success dropdown-toggle'
            to='#'
            role='button'
            id='dropdownMenu'
            data-bs-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            Sales
          </NavLink>
          <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
            <NavLink className='dropdown-item' to='/sales'>
              Sales History
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/customer'>
              Add new customer
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/employees'>
              Add a new Sales person
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/newsale'>
              Log a new sale
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/employeelog'>
              Filter sales by employee
            </NavLink>
              <li className='dropdown-divider' />
            <NavLink className='dropdown-item' to='/customerlist'>
              List Sold Customers
            </NavLink>
          </div>
        </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
