import React from 'react';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            customer_name : '',
            date_created : '',
            appointment_reason : '',
            vin : '',
            technician : '',
            technicians : [],

        };
    };

    handleInput = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name] : value});

    };

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state };
        delete data.technicians;

        const appointmentDetailURL = 'http://localhost:8080/api/service/appointments/';

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',},
        };

        const response = await fetch (appointmentDetailURL, fetchConfig);
        if (response.ok) {
            event.target.reset();
        };
        

    };
    async componentDidMount() {
        const url = 'http://localhost:8080/api/service/technicians';
    
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          this.setState({ technicians: data.technicians });
        }
      }


    render () {
        return (
            <div className='form'>
                <h1>Service Appointment</h1>
                <hr />
                <form onSubmit={this.handleSubmit}>
                    <div>
                        Name:
                        <input onChange={this.handleInput} type="text" placeholder='Customer Name' name='customer_name' />
                    </div>
                    <div>
                        Date:
                        <input onChange={this.handleInput} type="date" placeholder='Date' name='date_created' />
                    </div>
                    <div>
                        Appt. Reason:
                        <input onChange={this.handleInput}type="text" placeholder='Appt. Reason' name='appointment_reason'/>
                    </div>
                    <div>
                        VIN#:
                        <input onChange={this.handleInput} type="text" placeholder='VIN#' name='vin' />
                    </div>
                    <div>
                        Technician:
                        <select onChange={this.handleInput} type="text" name='technician' id='technician'>
                            <option value="">Choose Technician</option>
                            {this.state.technicians.map(technician => {
                                return (
                                    <option key={technician.employee_number} value={technician.employee_number}>{technician.name}</option>
                                )
                            } )}
                        </select>
                    </div>
                    <br />
                    
                    <input type='submit' value='Submit' />

                </form>

            </div>
        )
    };

};

export default Form;

