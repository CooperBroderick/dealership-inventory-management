from ast import Mod
from platform import java_ver
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

from .models import Appointments, AutomobileVO, Technician

# Create your views here.
class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties =["name", "employee_number"]

class AppointmentsListEncoder(ModelEncoder):
    model = Appointments
    properties = ["customer_name", "vin", "date_created", "appointment_reason", "vip", "technician",
    ]
    encoders= {"technician": TechnicianListEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians}, encoder=TechnicianListEncoder,)
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(technician, encoder=TechnicianListEncoder, safe=False,)

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointments.objects.all()
        return JsonResponse({"appointments": appointments}, encoder=AppointmentsListEncoder,)
    else:
        content = json.loads(request.body)
        try:
            content["technician"] = Technician.objects.get(employee_number=content["technician"])
        except Technician.DoesNotExist:
            return JsonResponse({"messege": "Invalid Employee"},
            status = 400,)
        try:
            id = content["vin"]
            vin = AutomobileVO.objects.get(vin=id)
            content["vip"] = True
        except AutomobileVO.DoesNotExist:
            content["vip"] = False
        appointment = Appointments.objects.create(**content)
        return JsonResponse(appointment, encoder=AppointmentsListEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointments.objects.get(id=pk)
        return JsonResponse(appointment, encoder=AppointmentsListEncoder, safe=False)
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "technician" in content:
                content["technician"] = Technician.objects.get(employee_number=content["technician"])
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does Not Exist"}, 
            status = 404,) 

        Appointments.objects.filter(id=pk).update(**content)
        appointment = Appointments.objects.get(id=pk)
        return JsonResponse(appointment, encoder=AppointmentsListEncoder, safe=False,)
    else:
        try:
            count, _ = Appointments.objects.filter(id=pk).delete()
            return JsonResponse({"DELETED": count > 0 })
        except Appointments.DoesNotExist:
            return JsonResponse({"message": "There's Nothing Here, Try Again!"})